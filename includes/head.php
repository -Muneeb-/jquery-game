<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Souls</title>
	<link rel="icon" type="image/png" href="img/icons/favicon.ico">
	<link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="css/maps/level-1.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/bootstrap-theme.css">
</head>
<body>