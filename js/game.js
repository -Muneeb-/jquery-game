// $(function(){

	// Put player on map
	// var player = '<div id="player" class="img-responsive"></div>';
	// $("#map").append(player);

	// Set layout for player controlls
	// $(document).keyup(function (e){
		// $("#player.img-responsive").removeClass("run-right");
	// });
	// $(document).keydown(function (e) {

		// Use this to identify what button the user is pressing on the keyboard
		// alert(e.keyCode);

		// get the position of the character
		// var position = $("#player").position();
		// $("#player.img-responsive").addClass("run-right");
		// switch (e.keyCode)
		// {
			// case 37:  // Arrow Left
				// $("#player").css(
						// 'left'		, position.left - 10 + 'px'
						// );
				// break;

			// case 38:  // Arrow Up
				// $("#player").css(
						// 'top'		, position.top - 10 + 'px'
						// );
				// break;

			// case 39:  // Arrow Right
				// $("#player").css(
						// 'left'		, position.left + 10 + 'px'
						// );
				// break;

			// case 40:  // Arrow Down
				// $("#player").css(
						// 'top'		, position.top + 10 + 'px'
						// );
				// break;

		//} // End Switch
	//}); // End KeyDown function
//}); // End Main function

// size of canvas
// document.getElementById("mainCanvas").style.width = '100%';

// var canvas_width = document.getElementById("mainCanvas").style.width = '100%';

var canvas = document.getElementById("mainCanvas");

var context = canvas.getContext("2d");

// keyboard layout
var keys = [];

var width  = 900, 
	height = 900, 
	speed  = 3;

var player = {
	x      : 10,
	y      : 10,
	width  : 10,
	height : 10
};

window.addEventListener("keydown", function (e){
	keys[e.keyCode] = true;
}, false);

window.addEventListener("keyup", function (e){
	delete keys[e.keyCode];
}, false);



function character()
{
  	base_image = new Image();
  	base_image.src = 'img/players/player.png';
  	base_image.onload = function() {
		context.drawImage(base_image, 100, 100);
  }
}

function game() {
	update();
	render();
}

function update(){

/* ++++++++*Legend*+++++++++++

		up arrow     = 38
		down arrow   = 40
		left arrow   = 37
		right arrow  = 39

++++++++++++++++++++++++++++ */

	/* 
		Directional keys
	*/
	if ( keys[38] ) { player.y-=speed; }
	if ( keys[40] ) { player.y+=speed; }
	if ( keys[37] ) { player.x-=speed; }
	if ( keys[39] ) { player.x+=speed; }
	/* 
		Keep player on map
	*/
	// left side
	if( player.x < 0 ) { player.x = 0; }

	// top side
	if( player.y < 0 ) { player.y = 0; }

	var rightSide = player.x + player.width;
	var bottomSide = player.y + player.height;

	if( rightSide >= width ) { player.x = ( width - 10 ); }
	if( bottomSide >= width ) { player.y = ( width - 10 ); }

}
function render() {
	context.clearRect( 0, 0, width, height );

	context.fillRect( player.x, player.y, player.width, player.height );

}
// set frame loop
setInterval(function(){
	game();
}, 1000/60);