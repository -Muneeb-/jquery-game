var increments = 10;

$(function(){

	// Put player on map
	var player = '<div id="player" class="img-responsive"></div>';
	$("#map").append(player);

	// Set layout for player controlls
	$(document).keyup(function (e){
		 e.preventDefault();
		$("#player.img-responsive").removeClass("run");
	});
	$(document).keydown(function (e) {
		 e.preventDefault();
		// Use this to identify what button the user is pressing on the keyboard
		// alert(e.keyCode);

		// get the position of the character
		var position = $("#player").position();
		$("#player.img-responsive").addClass("run");
		switch (e.keyCode)
		{
			case 37:  // Arrow Left
				if(position.left >= increments) {
					$("#player").css('left', position.left - increments + 'px');
				}
				break;

			case 38:  // Arrow Up
				if(position.top <= increments) {
					$("#player").css('top', position.top - increments + 'px');
				}
				break;

			case 39:  // Arrow Right
				if(position.left < 1000) { // Right constraint
					$("#player").css('left', position.left + increments + 'px');
				}
				break;

			case 40:  // Arrow Down
				if(position.top < 1000) { // Bottom constraint
					$("#player").css('top', position.top + increments + 'px');
				}
				break;

		} // End Switch
	}); // End KeyDown function
}); // End Main function